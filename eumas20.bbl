\begin{thebibliography}{10}
\providecommand{\url}[1]{\texttt{#1}}
\providecommand{\urlprefix}{URL }
\providecommand{\doi}[1]{https://doi.org/#1}

\bibitem{CoastGuard}
An, B., Ordóñez, F., Tambe, M., Shieh, E., Yang, R., Baldwin, C., DiRenzo,
  J., Moretti, K., Maule, B., Meyer, G.: A deployed quantal response-based
  patrol planning system for the u.s. coast guard. Interfaces  \textbf{43},
  400--420 (10 2013). \doi{10.1287/inte.2013.0700}

\bibitem{SUQR}
Brown, M., Haskell, W.B., Tambe, M.: Addressing scalability and robustness in
  security games with multiple boundedly rational adversaries. In: Decision and
  Game Theory for Security. pp. 23--42. Springer International Publishing, Cham
  (2014)

\bibitem{Conitzer}
Conitzer, V., Sandholm, T.: Computing the optimal strategy to commit to. In:
  Proceedings of the 7th ACM Conference on Electronic Commerce. pp. 82--90. EC
  '06, ACM, New York, NY, USA (2006). \doi{10.1145/1134707.1134717},
  \url{http://doi.acm.org/10.1145/1134707.1134717}

\bibitem{Fang}
Fang, F., Stone, P., Tambe, M.: When security games go green: Designing
  defender strategies to prevent poaching and illegal fishing. In: Proceedings
  of the 24th International Conference on Artificial Intelligence. pp.
  2589--2595. IJCAI'15, AAAI Press (2015),
  \url{http://dl.acm.org/citation.cfm?id=2832581.2832611}

\bibitem{GameTheory}
Fudenberg, D., Tirole, J.: Game Theory. MIT Press, Cambridge, MA (1991)

\bibitem{Kar:2015:GTH:2772879.2773329}
Kar, D., Fang, F., Delle~Fave, F., Sintov, N., Tambe, M.: "a game of thrones":
  When human behavior models compete in repeated stackelberg security games.
  In: Proceedings of the 2015 International Conference on Autonomous Agents and
  Multiagent Systems. pp. 1381--1390. AAMAS '15, International Foundation for
  Autonomous Agents and Multiagent Systems, Richland, SC (2015),
  \url{http://dl.acm.org/citation.cfm?id=2772879.2773329}

\bibitem{MovingAverage}
Kenney, J.F.: Mathematics of Statistics, vol.~1. Van Nostrand, 3rd edn. (1964)

\bibitem{Kiekintveld:2013:SGI:2484920.2484959}
Kiekintveld, C., Islam, T., Kreinovich, V.: Security games with interval
  uncertainty. In: Proceedings of the 2013 International Conference on
  Autonomous Agents and Multi-agent Systems. pp. 231--238. AAMAS '13,
  International Foundation for Autonomous Agents and Multiagent Systems,
  Richland, SC (2013), \url{http://dl.acm.org/citation.cfm?id=2484920.2484959}

\bibitem{Kocsis}
Kocsis, L., Szepesv\'{a}ri, C.: Bandit based monte-carlo planning. In:
  Proceedings of the 17th European Conference on Machine Learning. pp.
  282--293. ECML'06, Springer-Verlag, Berlin, Heidelberg (2006)

\bibitem{Letchford}
Letchford, J., Conitzer, V., Munagala, K.: Learning and approximating the
  optimal strategy to commit to. In: Algorithmic Game Theory. pp. 250--262.
  Springer Berlin Heidelberg, Berlin, Heidelberg (2009)

\bibitem{Marecki}
Marecki, J., Tesauro, G., Segal, R.: Playing repeated stackelberg games with
  unknown opponents. In: Proceedings of the 11th International Conference on
  Autonomous Agents and Multiagent Systems - Volume 2. pp. 821--828. AAMAS '12,
  International Foundation for Autonomous Agents and Multiagent Systems,
  Richland, SC (2012), \url{http://dl.acm.org/citation.cfm?id=2343776.2343814}

\bibitem{Martin}
Martín, A., Rodríguez-Fernández, V., Camacho, D.: Candyman: Classifying
  android malware families by modelling dynamic traces with markov chains.
  Engineering Applications of Artificial Intelligence  \textbf{74},  121 -- 133
  (2018). \doi{https://doi.org/10.1016/j.engappai.2018.06.006},
  \url{http://www.sciencedirect.com/science/article/pii/S0952197618301374}

\bibitem{Nguyen}
Nguyen, T.H., Yang, R., Azaria, A., Kraus, S., Tambe, M.: Analyzing the
  effectiveness of adversary modeling in security games. In: Proceedings of the
  Twenty-Seventh AAAI Conference on Artificial Intelligence. pp. 718--724.
  AAAI'13, AAAI Press (2013),
  \url{http://dl.acm.org/citation.cfm?id=2891460.2891560}

\bibitem{Nudelman}
Nudelman, E., Wortman, J., Shoham, Y., Leyton-Brown, K.: Run the gamut: A
  comprehensive approach to evaluating game-theoretic algorithms. In:
  Proceedings of the Third International Joint Conference on Autonomous Agents
  and Multiagent Systems - Volume 2. pp. 880--887. AAMAS '04, IEEE Computer
  Society, Washington, DC, USA (2004). \doi{10.1109/AAMAS.2004.238},
  \url{https://doi.org/10.1109/AAMAS.2004.238}

\bibitem{Pita:2008:DAP:1402795.1402819}
Pita, J., Jain, M., Marecki, J., Ord\'{o}\~{n}ez, F., Portway, C., Tambe, M.,
  Western, C., Paruchuri, P., Kraus, S.: Deployed armor protection: The
  application of a game theoretic model for security at the los angeles
  international airport. In: Proceedings of the 7th International Joint
  Conference on Autonomous Agents and Multiagent Systems: Industrial Track. pp.
  125--132. AAMAS '08, International Foundation for Autonomous Agents and
  Multiagent Systems, Richland, SC (2008),
  \url{http://dl.acm.org/citation.cfm?id=1402795.1402819}

\bibitem{Raffetseder}
Raffetseder, T., Kruegel, C., Kirda, E.: Detecting system emulators. In:
  Information Security. pp. 1--18. Springer Berlin Heidelberg, Berlin,
  Heidelberg (2007)

\bibitem{Sartea2}
Sartea, R., Farinelli, A.: A monte carlo tree search approach to active malware
  analysis. In: Proceedings of the Twenty-Sixth International Joint Conference
  on Artificial Intelligence, {IJCAI-17}. pp. 3831--3837 (2017).
  \doi{10.24963/ijcai.2017/535}, \url{https://doi.org/10.24963/ijcai.2017/535}

\bibitem{Wang2019}
Wang, B., Zhang, Y., Zhou, Z.H., Zhong, S.: On repeated stackelberg security
  game with the cooperative human behavior model for wildlife protection.
  Applied Intelligence  \textbf{49}(3),  1002--1015 (Mar 2019).
  \doi{10.1007/s10489-018-1307-y},
  \url{https://doi.org/10.1007/s10489-018-1307-y}

\bibitem{MalwareData}
Wei, F., Li, Y., Roy, S., Ou, X., Zhou, W.: Deep ground truth analysis of
  current android malware. In: International Conference on Detection of
  Intrusions and Malware, and Vulnerability Assessment (DIMVA'17). pp.
  252--276. Springer, Bonn, Germany (2017)

\bibitem{Williamson}
Williamson, S.A., Varakantham, P., Hui, O.C., Gao, D.: Active malware analysis
  using stochastic games. In: Proceedings of the 11th International Conference
  on Autonomous Agents and Multiagent Systems - Volume 1. pp. 29--36. AAMAS
  '12, International Foundation for Autonomous Agents and Multiagent Systems,
  Richland, SC (2012), \url{http://dl.acm.org/citation.cfm?id=2343576.2343580}

\end{thebibliography}
