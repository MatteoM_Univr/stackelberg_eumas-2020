% !TEX root = eumas20.tex

\section{Proposed Methodology}
\label{sec:method}
The Stackelberg game model can naturally encode many real world scenarios where agent interactions are not simultaneous and one agent can decide which actions to perform after observing the other agent's actions.  
However, there are cases in which such model may not accurately represent the dynamics of the interactions for the agents. In particular, we notice that there is a special class of scenarios where the agents involved in the interaction can decide to be either leader or follower, hence switching roles during the game.
% For example, in past works on contrasting poaching~\cite{Fang,Wang2019}, the park ranger force is considered the leader player that decides which areas to patrol. However, there is also the other side of the coin where park rangers could observe the poaching activities to collect information and then determine in which area to perform an arrest blitz (hence acting as followers). Such observation translates in an extension of the previous anti-poaching models allowing to represent heterogeneous behaviors of the keeper agent.
To capture these scenarios we propose the Stackelberg Switch Role Game (\gls{ac:ssrg}) model, that we define as follows:

% consider a network expert that must detect a \gls{ac:dos}. She first observes how the server reacts under a normal traffic load and then generates an ad-hoc sequence of packets to verify its response in case of \gls{ac:dos}. In this case the server analyst acts as a follower waiting to achieve enough data about the server functionalities before executing her strategy, i.e.\ the \acrshort{ac:dos} attack simulation. However, the expert could, at a given point, choose to stop simulating a \acrshort{ac:dos} attacks and try to disclose the server functionalities by sending normal request packets. In this case, the network expert would plays as a leader committing a query strategy (i.e., which packets to send). Such observation applies also to previously studied domains. For instance, 

%A relevant property that is common to many real multi-agent scenarios consists in the asynchrony of the actions of the involved players, namely they are not simultaneous since an agent decides which actions to do just after the observation of the others' policies.  Typically such settings imply that the players do not know each other and thus need multiple subsequent interactions to retrieve knowledge about other agents. The combination of the two described features can be formally modeled through a repeated Stackelberg game where it is assumed that initially the agent rewards are random or unreliable and as the game progresses they are updated to refine.

%mutate its role. Considering such scenarios as Stackelberg games, this feature translates formally in the property that the involved players can exchange their types between the game rounds, i.e.\ the leader agent can become the follower and the follower undergoes to the opposite transformation.

\begin{definition}[\acrfull{ac:ssrg}]
\label{def:ssrg}
Let $p_1, p_2$ be a couple of agents and $R = \{(p_1 \leftarrow lead, p_2 \leftarrow follow), (p_1 \leftarrow follow, p_2 \leftarrow lead)\}$ the set of possible leader-follower role assignments. Suppose that the agent $p_1$ can arbitrarily select a role allocation $r \in R$.
% $Bern(x)$ be a Bernoulli distribution defined by probability $x$ with image
A \gls{ac:ssrg} is defined as a repeated Stackelberg game $s = (s_1, s_2, \dots, s_n)$ of length $n \in \mathbb{N}$, where each $s_i = (r, A_l, A_f, u_l, u_f)$ is a single Stackelberg game such that:
\begin{itemize}
\item $r \in R$ is the role assignment chosen by $p_1$ for the $i$-th stage
\item $A_l$ is the set of pure strategies of the leader
\item $A_f$ is the set of pure strategies of the follower
\item $u_l: A_l \times A_f \rightarrow \mathbb{R}$ is the leader's utility function
\item $u_f: A_l \times A_f \rightarrow \mathbb{R}$ is the follower's utility function
\end{itemize}
\end{definition}

We call the agent that controls the role switch (i.e., $p_1$) the \emph{master} and the procedure it uses to choose in each round the role allocation \emph{\acrfull{ac:rsp}}\footnote{In general, the choice about the employed \acrshort{ac:rsp} might be not determined by one agent, since it can be affected by environmental aspects or multiple agents}. In our scenarios, the master agent is decided a priori, without any chance for the involved agents to affect such choice. We decided to make this assumption because it captures well the malware analysis domain we are interested in. The definition above implies that whenever an agent $p_j$ switches between leader and follower, the achievable payoffs may change accordingly, since the $u_l$ and $u_f$ are related to the role assumed by $p_j$. In our \gls{ac:ssrg} model, the aim for the agents is to maximize the global average reward achievable at the end of the process.

\subsection{Assumptions and complexity results}
\label{sec:gameFeatures}
We assume that the payoff values for leader and follower are initially unknown to the agents: players maintain an estimation about the rewards induced by the action profiles that is updated as the game progresses. This core assumption is justified by the characteristics related to real-world multi-agent systems, as players typically are affected by incomplete knowledge regarding several key traits of the game setting: the most common shortcomings are represented by lack of information about strategies and revenues of other agents and incapability to accurately evaluate a reliable reward function for themselves. % Even if a player $p$ is fully aware about the game setting, it is counterproductive to optimally model $p$ if it interacts with bounded rational agents. Indeed $p$ develops a strategy assuming the others will play optimally as well, but since opponents may deviate from such supposition, its policy may result in poor rewards if such assumption does not hold.

% As mentioned in section~\ref{sec:background}, a line of research has tried to overcome these limitations by preliminary steps to the game resolution in order to lessen their magnitude, but they assume to result in approximated outcomes~\cite{}.

There are also computational limits that do not allow to extract the best solution of a \gls{ac:ssrg}. In particular, we demonstrate through corollaries~\ref{cor:hardness1} and \ref{cor:hardness2} that for any \gls{ac:ssrg} instance such that there is at least a not uniquely defined payoff value for an action profile, the problem of selecting the optimal policy for the master agent in each \gls{ac:ssrg} round is NP-hard. In order to prove such statement we leverage on the theorems $5$ and $7$ given in~\cite{Conitzer} which refer to \gls{ac:bsg}. Indeed, we show that a similar result holds in our setting by giving a procedure to reduce a \gls{ac:ssrg} round to an equivalent instance of \gls{ac:bsg}.

\begin{corollary} \label{cor:hardness1}
Finding an optimal mixed strategy to commit to in a round of a 2-player \acrshort{ac:ssrg} is NP-hard if the leader agent is uncertain about the follower rewards and the follower has at least two actions, even when the leader is fully aware about its rewards.
\end{corollary}

\begin{proof}
We show that an instance of single round a \acrshort{ac:ssrg} can be reduced to a corresponding instance of a \gls{ac:bsg}. Accomplishing such goal, we leverage on theorems~$7$ reported in~\cite{Conitzer} to prove corollary~1.

Suppose w.l.o.g.\ that we are given an instance of a 2-player \acrshort{ac:ssrg} where the leader is uncertain between two different payoff values for a given joint leader-follower action profile (due its limited knowledge about such agent). Then we can reformulate equivalently this uncertainty defined over the outcome of such action profile by splitting explicitly into two distinct payoff matrices. The two created matrices can be interpreted as two behavioral types of follower. Therefore we obtain an instance of \gls{ac:bsg} where the leader has a single type and follower has two types. Hence theorem~$7$ of~\cite{Conitzer} applies.
\end{proof}

\begin{corollary} \label{cor:hardness2}
Finding an optimal mixed strategy to commit to in a round of a 2-player \gls{ac:ssrg} is NP-hard if the leader agent is uncertain about its rewards, even when the leader perfectly knows the follower rewards.
\end{corollary}

\begin{proof}
Using the same approach described for the proof of corollary~1, we can obtain a transformation of any \acrshort{ac:ssrg} to an equivalent \gls{ac:bsg} instance where we have at least two types (payoff functions) defined for the leader. Hence, the obtained instance matches the hypothesis of theorem~$5$ shown in~\cite{Conitzer}, which states the NP-hardness of committing an optimal pure strategy: since the pure strategies of the leader is a subset of the possible mixed strategies, such NP-hardness result applies also generally to mixed strategies.
\end{proof}

%\begin{table}[!hbt]
%\centering
%\caption{Example payoff matrix held by the leader}
%\label{tab:uncertainPayoff}
%\begin{tabular}{|c|c|}
%\hline
%$2, \pm3$ & $-3, 1$ \\
%\hline
%$0, -3$ & $2, -1$ \\
%\hline
%\end{tabular}
%\end{table}

%\begin{table}[!hbt]
%\centering
%\caption{The two splitted payoff matrices induced by table~\ref{tab:uncertainPayoff}, representing two distinct follower types}
%\label{tab:splitPayoffs}
%\begin{tabular}{|c|c|}
%\hline
%$2, 3$ & $-3, 1$ \\
%\hline
%$0, -3$ & $2, -1$ \\
%\hline
%\end{tabular}
%\begin{tabular}{|c|c|}
%\hline
%$2, -3$ & $-3, 1$ \\
%\hline
%$0, -3$ & $2, -1$ \\
%\hline
%\end{tabular}
%\end{table}

Whenever an agent behaves as follower, there are no complexity limitations in computing the optimal response to a committed mixed strategy, since such task requires a polynomial time for the resolution. However, if the master decides to address an \acrshort{ac:ssrg} interpreting the follower in every round to play optimally (and avoiding the burden of finding optimal mixed strategies), it may miss the opportunity to achieve a greater reward, since the leader sub-game might present more attractive payoffs. Thus the master would not obtain optimal overall results.
% Since, on the long term, the amount of iterations in \acrshort{ac:ssrg} in which every player assumes the leader role is greater than $0$, they do not behave optimally overall (except in extreme cases where the probability distribution to switch the roles is absolute, i.e.\ $0$ for all role allocation entries but $1$ for a given assignment).

% While we demonstrate that a single turn of the \acrshort{ac:ssrg} we consider is reducible to a \gls{ac:bsg}, it is worth to notice that the \acrshort{ac:ssrg} definition turns out to be a compact representation of the problem case. Indeed as the degree of uncertainty about the follower payoffs (i.e.\ the number of action profiles presenting alternative reward values the leader can potentially assign) increases, the number of follower types grows exponentially. Let $U_1$, $U_2$, ..., $U_n$ be the sets of potential payoff values for different action profiles $a_1$, $a_2$, ..., $a_n$. The number of follower types induced by transforming such Stackelberg game into a \gls{ac:bsg} where $U_1$, $U_2$, ..., $U_n$ are defined is equal to the cardinality of the cartesian product $U_p = U_1 \times U_2 \times ... \times U_n$. Since each set $U_i$ is composed by at least two elements, we have that a lower bound for $|U_p|$ is $2^n$. Hence, describing a Stackelberg game iteration of a \acrshort{ac:ssrg} as a \gls{ac:bsg} might require an intractable amount of space.

In the next section, we focus on a sub-optimal solution approach assuming that the agent we design is the master in the \gls{ac:ssrg}.

\subsection{Solution Method: \acrshort{ac:safe}}
\label{sec:framework}
The main elements to devise a solution procedure for a \gls{ac:ssrg} are the following: a) a routine to determine, at the beginning of each round, which leader-follower assignment is more profitable for the master; b) a method to compute the master strategy for both the roles; c) a procedure to update the estimated rewards w.r.t.\ the actual reward achieved in previous rounds.
In figures~\ref{fig:overviewLeader} and \ref{fig:overviewFollower}, we show a scheme of \gls{ac:safe}, the framework we design to handle \gls{ac:ssrg}: first, a routine chooses the role assumed by each player in the next round; then a Stackelberg game round is performed between two bounded rational agents, producing expected and actual payoffs that update respectively the behavioral model of the adversary the master maintains and the reward estimation of the master for the chosen joint action profile. Notice that, in our formulation, we assume that the opponent which interacts with the master (our agent) behaves myopically employing non-strategic best responses. Hence it does not implement an intelligent strategy to infer the policy of the master to counter: when it interprets the follower role, it limits to raise the best response action according to the rewards estimated currently; in case it is the leader, it just commits the best mixed strategy. Eventually the actual reward received is handled to update the master rewards estimations accordingly so as to drive the next game iterations.

\begin{figure}
\centering
\includegraphics[width=8.4cm]{images/LeaderFramework.pdf}
\caption{Overview of \gls{ac:safe} (master acting as leader)}
\label{fig:overviewLeader}
\end{figure}

%\begin{figure}
%\centering
%\includegraphics[width=8.4cm]{images/FollowerFramework.pdf}
%\caption{Overview of \gls{ac:safe} (master acting as follower)}
%\label{fig:overviewFollower}
%\end{figure}

\subsubsection{\gls{ac:rsp}} 
\label{sec:switch}
As it can be seen from figures~\ref{fig:overviewLeader} and \ref{fig:overviewFollower}, the keystone of the framework is represented by the block that assigns, at each round of the \gls{ac:ssrg}, the leader and follower roles to the agents. The implementation of this routine shapes the evolution of the game, affecting the potential reward score that both players can achieve and the kind of interaction that takes place\footnote{The leader can have a different action set w.r.t. the follower. Similarly the reward function of an agent can vary if it plays as a leader or as a follower.}.

In this work, we propose a first heuristic implementing the \gls{ac:rsp} that analyzes past rounds of the \gls{ac:ssrg} to return in output a probability distribution defined over the possible leader-follower allocations for the agents. Once the \gls{ac:rsp} generates such distribution, it is used to select the assignment leading the next round accordingly.

In order to explain the modeling features we required to embed in the \gls{ac:rsp}, we sketch an example scenario. Let's assume that the \gls{ac:ssrg} is at iteration $n$ and that in the previous round $n-1$ the master has received a high value payoff. Then our speculation would be to encourage for the round $n$ the same role assignment decided for $n-1$, since it has previously produced a good result. Conversely, we would aim at switching the roles if at step $n-1$ the master obtained a poor reward, expecting that such change might improve the master revenue. In other words, we state that it is worth to exchange the roles only when the master gains a payoff that it does not consider sufficient.
Moreover, there is another additional property that we want to insert in the \gls{ac:rsp}: the output probability distribution should never present $0$ likelihoods for any role allocation, unless such assignments are proven to be detrimental for our agent.
% Indeed, the task handled by \gls{ac:rsp} recalls a multi-armed bandit problem, seeking a tradeoff between exploration and exploitation of leader-follower assignments: if any role allocation has probability $0$ to be chosen in the distribution, we restrict the exploration space, causing a potential deterioration of the achievable reward.

%\begin{algorithm}
%	\caption{\textsc{\acrfull{ac:rsp}}}
%	\begin{algorithmic}[1]
%		\Require
%		\Statex $r$ - expected reward obtained at step $n-1$
%		\Statex $a$ - leader-follower assignment at step $n-1$
%		\Statex $c$ - constant value
%		\Ensure
%		\Statex $k$ - a leader-follower allocations for round $n$
%		\Statex
%		\If{$n > 0$}
%			\State $update \gets \frac{c\cdot \log{(r+1)}}{1 + c\cdot \log{(r+1)}}$ \label{alg:line1} \Comment{$c$ tunes the update var.}
%			\State $D \gets (a \gets update, \bar{a} \gets 1 - update)$ \label{alg:rest1}
%			\State $k \gets$ \Call{Random}{$D$} \label{alg:rest2}%\Comment{Select between $a$ and $\bar{a}$}
%			\State \Return $k$	\label{alg:rest3}
%		\Else \Comment{1st \gls{ac:ssrg} round}
%			\State \Return \Call{Random}{$a, \bar{a}$} \label{alg:firstRound} \Comment{Use uniform distribution}
%		\EndIf
%	\end{algorithmic}
%	\label{alg:rsp}
%\end{algorithm}

\begin{algorithm}
	\caption{\textsc{\acrfull{ac:rsp}}}
	\begin{algorithmic}[1]
		\Require
		\Statex $r$ - expected reward obtained at step $n-1$
		\Statex $a$ - leader-follower assignment at step $n-1$
		\Statex $c$ - constant value
		\Ensure
		\Statex A leader-follower allocation for round $n$
		\Statex
		\If{$n > 0$}
			\State $update \gets \frac{c\cdot \log{(r+1)}}{1 + c\cdot \log{(r+1)}}$ \label{alg:line1} %\Comment{Compute probability of taking role $a$ again}
			\State $a \gets update, \bar{a} \gets (1 - update)$ \label{alg:rest1}
		\Else \Comment{1st \gls{ac:ssrg} round}
		    \State $a \gets 0.5, \bar{a} \gets (1 - a)$ \label{alg:firstRound} \Comment{Use uniform distribution}
		\EndIf
		\State \Return \Call{Random}{$a, \bar{a}$} \label{alg:return}
	\end{algorithmic}
	\label{alg:rsp}
\end{algorithm}

We propose algorithm~\ref{alg:rsp} to implement the \gls{ac:rsp} for \gls{ac:ssrg}s we described in subsection~\ref{sec:gameFeatures}, considering the previous requirements. The heuristic is based only on the expected reward $r$ obtained in the last round, since we assume that it is the most reliable estimation of the actual payoff obtainable playing the sub-game specified by role allocation $a$. In contrast, if \gls{ac:ssrg} is at the starting iteration, we just sample the uniform distribution defined over the set of possible leader-follower assignments (line~\ref{alg:firstRound}). In the non-trivial case (lines~\ref{alg:line1}-\ref{alg:rest1}), the fundamental point is given in line~\ref{alg:line1}, where a probability value to associate to $a$ is computed, representing how attractive it appears for the master agent to maintain the $a$ role in the next round. The mathematical function we suggest fits the features we described, since it incentives $a$ if it has induced a high reward in the previous round, whilst penalizes it in the opposite case\footnote{The logarithm has been employed because it has a smooth trend; $r$ is summed to $1$ in the logarithm to avoid undefined results.}. Moreover, the likelihoods associated to the two possible role assignments will never be equal to $0$, no matter how large or small $r$ is, thus preventing $0$ probabilities for any allocation.
Once the update value for $a$ is computed, the probability coupled to $\bar{a}$, the other possible allocation, is defined as its complement. Finally, line \ref{alg:return} randomly selects the next role according to the probability distribution between $a$ and $\bar{a}$.

\subsubsection{\acrfull{ac:mcts}}
\label{sec:mcts}
The playing strategy for the master agent has been realized adopting the \gls{ac:mcts}. Our implementation of the \gls{ac:mcts} searches among the set of pure strategies (actions) defined for the role held by the master. The selection, expansion and backpropagation steps are based on the \gls{ac:uct}~\cite{Kocsis}. The tree policy proposed with \gls{ac:uct} addresses the exploration-exploitation dilemma to balance between the selection of actions that have not been well sampled yet and the promising ones already executed in previous iterations. Another key aspect of our \gls{ac:mcts} version is represented by the simulation function. The master agent incrementally refines a behavioral model of the adversary expressing the relationship between joint action profiles issued and the associated rewards: the simulation step of \gls{ac:mcts} uses the information contained in such model to predict the response of the opponent agent. We implement such routine with the same approach presented in~\cite{Sartea2}, both to shape the agent model and to perform the simulation.

When the \gls{ac:mcts} procedure terminates, we obtain a structure where at tree depth $1$ there are all the nodes belonging to the action set of the master. In order to construct a suitable strategy, we employ only the simulated reward values embedded in the children of the root. If the master decides to play as leader in this round, then the mixed strategy it will commit is built by normalizing to $1$ the rewards accumulated in such nodes. This operation converts the simulated unbounded rewards contained in the children of the root into a corresponding probability distribution (defined over the actions labelling the considered nodes, i.e.\ master action set), which results to be the mixed strategy for the master. Otherwise, if the master decides to play as follower, it will choose as pure strategy the action with highest simulated reward among the same subset of the nodes. For example, in figure~\ref{fig:mcts}e the blue nodes are children of the root used to compute the master policy. If the master is the leader, we convert the rewards (upper image) normalizing to $1$, obtaining the depicted mixed strategy (lower image); otherwise, it issues the pure strategy $A3$ since it achieved the highest reward.

If the agent running the \gls{ac:mcts} is aware about all the information needed (no uncertainty or noise) then the \gls{ac:mcts} would lead to the optimal mixed strategy asymptotically (see subsection~\ref{sec:mctsBack}). However, due to the limited amount of time the \gls{ac:mcts} executes and the incomplete/noisy information available to our agent, it produces approximated strategies. It is worth to underline that we divided the \gls{ac:rsp} and the algorithm generating the strategy of the master because, whenever the master plays as follower, these two steps are inherently separated by the observation of the leader committed policy. % must first observe the adversary strategy and then use the acquired information to devise its own strategy. If the master computes both the role switch and the strategy in one step, it would not be possible to use the knowledge acquired observing the leader.

\subsubsection{Reward estimation}
\label{sec:beliefs}
As we mentioned earlier, both players begin an \gls{ac:ssrg} without knowing any true payoff value for any action profile. Therefore, the agents need to maintain estimates of the actual rewards for themselves and for the opponents to devise a proper strategy. Initially the agents fix the reward estimation for all action combinations to an uninformative constant: since all the actions produce the same value w.r.t. such estimation, they are evaluated likewise. However, as the \gls{ac:ssrg} progresses, the players receive reward signals that are useful to revise such estimation and to learn the actual game payoffs.

In order to implement such refinement process of the agent reward estimation, we rely on the widely used concept of moving average~\cite{MovingAverage}. In particular we employ the exponential moving average as a mathematical tool to update the reward estimation as the tradeoff obtained balancing the actual reward received by the master at the end of game iteration $n$ and the estimation computed at the previous round $n-1$. Such update function produces the effect of weighting the first mentioned term most in the early stages of the \gls{ac:ssrg}, since the reward estimation held by the master is considered inaccurate. However, as the game advances, the agent reward estimation tends to converge to the actual reward set for the \gls{ac:ssrg} instance.